package com.nkorth.notepad;

import java.util.Calendar;
import java.util.GregorianCalendar;

import android.app.AlertDialog;
import android.app.ListFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.format.DateUtils;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.ListView;
import android.widget.ShareActionProvider;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

public class TextListFragment extends ListFragment {
	
	private SQLiteDatabase db;
	private Cursor query_result;
	private SimpleCursorAdapter adapter;
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		setEmptyText(getResources().getString(R.string.no_notes));
		
		DBStructure dbStructure = new DBStructure(getActivity());
		db = dbStructure.getReadableDatabase();
		requeryNotes();
		adapter = new SimpleCursorAdapter(getActivity(), R.layout.simple_list_item_activated_2, query_result,
				new String[]{DBStructure.CONTENT, DBStructure.TIME}, new int[]{android.R.id.text1, android.R.id.text2}, 0);
		adapter.setViewBinder(new SimpleCursorAdapter.ViewBinder(){
			@Override
			public boolean setViewValue(View v, Cursor c, int columnIndex){
				String name = c.getColumnName(columnIndex);
				if(name.equals(DBStructure.CONTENT)){
					String content = c.getString(columnIndex);
					TextView view = (TextView) v;
					if(content.indexOf("\n") > 0){
						view.setText(content.substring(0, content.indexOf("\n")));
					} else{
						view.setText(content);
					}
					return true;
				} else if(name.equals(DBStructure.TIME)){
					TextView view = (TextView) v;
					long unixTime = c.getLong(columnIndex);
					Calendar time = new GregorianCalendar();
					time.setTimeInMillis(unixTime * 1000);
					view.setText(relativeTimeString(time));
					return true;
				}
				return false;
			}
			
			private String relativeTimeString(Calendar time){
				Calendar now = Calendar.getInstance();
				int flags = DateUtils.FORMAT_NO_NOON
						  | DateUtils.FORMAT_NO_MIDNIGHT;
				if(now.get(Calendar.YEAR) != time.get(Calendar.YEAR)){
					// different year
					flags |= DateUtils.FORMAT_SHOW_DATE
						   | DateUtils.FORMAT_NO_MONTH_DAY;
				} else if(now.get(Calendar.DAY_OF_YEAR) - time.get(Calendar.DAY_OF_YEAR) > 6){
					// same year, but at least one week old
					flags |= DateUtils.FORMAT_SHOW_DATE;
				} else if(now.get(Calendar.DAY_OF_YEAR) != time.get(Calendar.DAY_OF_YEAR)){
					// within the week, but different day
					flags |= DateUtils.FORMAT_SHOW_WEEKDAY;
				} else{
					// same day
					flags |= DateUtils.FORMAT_SHOW_TIME;
				}
				long millis = time.getTimeInMillis();
				return DateUtils.formatDateTime(getActivity(), millis, flags);
			}
		});
		setListAdapter(adapter);
		
		// set up contextual action bar
		getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
		getListView().setMultiChoiceModeListener(new MultiChoiceModeListener() {
			
			MenuItem share;
			
			@Override
			public boolean onCreateActionMode(ActionMode mode, Menu menu) {
				MenuInflater inflater = getActivity().getMenuInflater();
				inflater.inflate(R.menu.list_context, menu);
				share = menu.findItem(R.id.action_share);
				return true;
			}
			
			@Override
			public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
				return false;
			}
			
			@Override
			public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
				switch(item.getItemId()){
					case R.id.action_delete:
						confirmDeletion(mode);
						return true;
					default:
						return false;
				}
			}
			
			private Intent getShareIntent(){
				SparseBooleanArray array = getListView().getCheckedItemPositions();
				int position = -1;
				for(int i = 0; i < array.size(); i++){
					if(array.valueAt(i)){
						position = array.keyAt(i);
						break;
					}
				}
				SQLiteCursor cursor = (SQLiteCursor) adapter.getItem(position);
				String content = cursor.getString(cursor.getColumnIndex(DBStructure.CONTENT));
				Intent i = new Intent(Intent.ACTION_SEND);
				i.setType("text/plain");
				i.putExtra(Intent.EXTRA_TEXT, content);
				return i;
			}
			
			@Override
			public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
				int count = getListView().getCheckedItemCount();
				mode.setTitle(getActivity().getString(R.string.num_selected, count));
				if(count == 1){
					share.setEnabled(true);
					share.setVisible(true);
					((ShareActionProvider) share.getActionProvider()).setShareIntent(getShareIntent());
				} else{
					share.setEnabled(false);
					share.setVisible(false);
				}
			}

			@Override
			public void onDestroyActionMode(ActionMode mode) {
			}
		});
	}
	
	@Override
	public void onResume() {
		super.onResume();
		requeryNotes();
	}

	private void requeryNotes() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
		String sortMode = prefs.getString(SettingsActivity.KEY_SORT_MODE, "updated");
		
		String orderBy;
		if(sortMode.equals("alphabetical")){
			orderBy = DBStructure.CONTENT + " collate nocase";
		} else if(sortMode.equals("created")){
			orderBy = DBStructure.ID;
		} else{
			orderBy = DBStructure.TIME;
		}
		if(prefs.getBoolean(SettingsActivity.KEY_SORT_DESCENDING, true)){
			orderBy += " desc";
		}
		
		query_result = db.query(DBStructure.NOTES_TABLE,
				null,
				null, null,
				null, null, orderBy);
		if(adapter != null){
			adapter.changeCursor(query_result);
		}
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id){
		Intent i = new Intent(getActivity(), TextEditActivity.class);
		i.putExtra(TextEditActivity.EXTRA_ID, id);
		startActivity(i);
	}
	
	private void confirmDeletion(final ActionMode mode){
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		
		int count = getListView().getCheckedItemCount();
		String message = getActivity().getString(R.string.delete_confirmation);
		if(count != 1){
			message = getActivity().getString(R.string.delete_confirmation_plural, count);
		}
		builder.setMessage(message);
		
		builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener(){
			@Override
			public void onClick(DialogInterface dialog, int which){
				dialog.cancel();
			}
		});
		builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener(){
			@Override
			public void onClick(DialogInterface dialog, int which){
				deleteSelectedNotes();
				mode.finish();
				requeryNotes();
				dialog.cancel();
			}
		});
		
		builder.create().show();
	}
	
	private void deleteSelectedNotes(){
		for(Long id : getListView().getCheckedItemIds()){
			db.delete(DBStructure.NOTES_TABLE, DBStructure.ID+" = ?", new String[]{id.toString()});
		}
	}
	
	@Override
	public void onDestroy(){
		query_result.close();
		db.close();
		super.onDestroy();
	}
	
}
