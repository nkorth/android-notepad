package com.nkorth.notepad;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class TextEditActivity extends Activity {

	public static final String EXTRA_ID = "id";
	
	private TextEditFragment fragment;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if(PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsActivity.KEY_DARK_THEME, false)){
        	setTheme(R.style.DarkTheme);
        }
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_text_edit);
		
		FragmentManager fm = getFragmentManager();
		fragment = (TextEditFragment)fm.findFragmentById(R.id.placeholder);
		if(fragment == null){
			if(getIntent().hasExtra(EXTRA_ID)){
				fragment = TextEditFragment.newInstance(getIntent().getLongExtra(EXTRA_ID, -1));
			} else if(getIntent().hasExtra(Intent.EXTRA_TEXT)){
				fragment = TextEditFragment.newInstance(getIntent().getStringExtra(Intent.EXTRA_TEXT));
			} else{
				fragment = new TextEditFragment();
			}
			fm.beginTransaction().add(R.id.placeholder, fragment).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
    	inflater.inflate(R.menu.editor, menu);
    	return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		switch(item.getItemId()){
		case R.id.action_reminder:
			fragment.reminder();
			return true;
		default:
			return false;
		}
	}
	
}
