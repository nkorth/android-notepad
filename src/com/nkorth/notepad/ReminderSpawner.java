package com.nkorth.notepad;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.RingtoneManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.util.Log;

public class ReminderSpawner extends BroadcastReceiver {
	public static final String ACTION_REMINDER_NOTIFICATION = "com.nkorth.notepad.action_reminder_notification";
	public static final String EXTRA_NOTE_ID = "note_id";
	public static final String EXTRA_NOTE_TITLE = "note_title";

	@Override
	public void onReceive(Context context, Intent i) {
		if(i.getAction().equals(Intent.ACTION_BOOT_COMPLETED)){
			SQLiteDatabase db = (new DBStructure(context)).getReadableDatabase();
			Cursor cur = db.query(DBStructure.NOTES_TABLE, null, DBStructure.REMINDER+" is not null", null, null, null, null);
			if(cur.moveToFirst()){
				do {
					Note n = new Note(cur);
					if(n.getReminder() != null){
						Log.i("ReminderSpawner", n.getTitle());
						setReminder(context, n);
					}
				} while(cur.moveToNext());
			}
		} else if(i.getAction().equals(ACTION_REMINDER_NOTIFICATION)){
			long noteId = i.getLongExtra(EXTRA_NOTE_ID, -1);
			String noteTitle = i.getStringExtra(EXTRA_NOTE_TITLE);
			if(noteTitle == null) noteTitle = "";
			
			Intent showNote_i = new Intent(context, TextEditActivity.class);
			showNote_i.putExtra(TextEditActivity.EXTRA_ID, noteId);
			PendingIntent showNote_pi = PendingIntent.getActivity(context, (int) noteId, showNote_i, 0);
			
			Notification.Builder builder = new Notification.Builder(context)
				.setSmallIcon(R.drawable.ic_stat_alarm)
				.setContentTitle("Notepad reminder")
				.setContentText(noteTitle)
				.setContentIntent(showNote_pi)
				.setAutoCancel(true)
				.setDefaults(Notification.DEFAULT_LIGHTS);
			
			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
			String ringtoneString = prefs.getString(SettingsActivity.KEY_REMINDER_RINGTONE, "default");
			if(!ringtoneString.equals("")){
				Uri ringtone = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
				if(!ringtoneString.equals("default")){
					ringtone = Uri.parse(ringtoneString);
				}
				builder.setSound(ringtone);
			}
			
			NotificationManager notifications = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
			notifications.notify((int) noteId, builder.build());
			
			SQLiteDatabase db = (new DBStructure(context)).getWritableDatabase();
			Cursor cur = db.query(DBStructure.NOTES_TABLE, null,
					DBStructure.ID+" = ?", new String[]{""+noteId},
					null, null, null);
			if(cur.moveToFirst()){
				Note note = new Note(cur);
				note.setReminder(null);
				note.save(db);
			} else{
				Log.wtf("ReminderSpawner", "Note id was not found? "+noteId);
			}
			db.close();
		}
	}
	
	public static void setReminder(Context context, Note note){
		AlarmManager alarms = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		
		PendingIntent pi = note.getReminderPendingIntent(context);
		alarms.setExact(AlarmManager.RTC_WAKEUP, note.getReminder().getTime(), pi);
	}

}
