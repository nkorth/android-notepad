package com.nkorth.notepad;

import java.util.Date;

import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class Note {
	public Note(){
		time = new Date();
	}
	
	public Note(Cursor row) {
		id = row.getLong(row.getColumnIndex(DBStructure.ID));
		content = row.getString(row.getColumnIndex(DBStructure.CONTENT));
		time = new Date(row.getLong(row.getColumnIndex(DBStructure.TIME)) * 1000);
		if(!row.isNull(row.getColumnIndex(DBStructure.REMINDER))){
			reminder = new Date(row.getLong(row.getColumnIndex(DBStructure.REMINDER)) * 1000);
		}
	}

	private Long id;
	private String content;
	private Date time;
	private Date reminder;
	
	public String getContent(){
		if(content == null){
			return "";
		}
		return content;
	}
	public void setContent(String content){
		if(content.equals(this.content)) return;
		this.content = content;
		this.time = new Date();
	}
	
	public String getTitle(){
		String content = getContent();
		if(content.indexOf('\n') > 0){
			return content.substring(0, content.indexOf('\n'));
		} else{
			return content;
		}
	}
	
	public Date getReminder(){
		if(reminder != null && reminder.before(new Date())){
			reminder = null;
		}
		return reminder;
	}
	public void setReminder(Date reminder){
		this.reminder = reminder;
	}
	
	public PendingIntent getReminderPendingIntent(Context context){
		Intent notification_i = new Intent(context, ReminderSpawner.class);
		notification_i.setAction(ReminderSpawner.ACTION_REMINDER_NOTIFICATION);
		notification_i.putExtra(ReminderSpawner.EXTRA_NOTE_ID, id);
		notification_i.putExtra(ReminderSpawner.EXTRA_NOTE_TITLE, getTitle());
		PendingIntent notification_pi = PendingIntent.getBroadcast(context, id.intValue(), notification_i, 0);
		return notification_pi;
	}
	
	public void save(SQLiteDatabase db){
		if(getContent().equals("")){
			// database shouldn't contain any empty notes
			if(id != null){
				// delete the note if it has been saved before
				db.delete(DBStructure.NOTES_TABLE, DBStructure.ID+"=?", new String[]{id.toString()});
				id = null;
			}
			return;
		}
		
		ContentValues values = new ContentValues();
		values.put(DBStructure.CONTENT, content);
		values.put(DBStructure.TIME, time.getTime() / 1000);
		if(getReminder() != null){
			values.put(DBStructure.REMINDER, getReminder().getTime() / 1000);
		} else{
			values.putNull(DBStructure.REMINDER);
		}
		
		if(id == null){
			id = db.insert(DBStructure.NOTES_TABLE, null, values);
		} else{
			values.put(DBStructure.ID, id);
			db.replace(DBStructure.NOTES_TABLE, null, values);
		}
	}
}
