package com.nkorth.notepad;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

public class SettingsActivity extends PreferenceActivity {
	public static final String KEY_SORT_MODE = "sortNotesMode";
	public static final String KEY_SORT_DESCENDING = "sortNotesDescending";
	public static final String KEY_DARK_THEME = "darkTheme";
	public static final String KEY_REMINDER_RINGTONE = "reminderRingtone";
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		if(PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingsActivity.KEY_DARK_THEME, false)){
        	setTheme(R.style.DarkTheme);
        }
		
		super.onCreate(savedInstanceState);
		
		addPreferencesFromResource(R.xml.preferences);
	}
}
