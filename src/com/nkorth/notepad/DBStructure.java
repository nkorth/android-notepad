package com.nkorth.notepad;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBStructure extends SQLiteOpenHelper {

	private Context context;
	
	public DBStructure(Context context) {
		super(context, DB_NAME, null, VERSION);
		this.context = context;
	}
	
	public static final int VERSION = 2;
	public static final String DB_NAME = "notepad";
	public static final String NOTES_TABLE = "notes";
	public static final String ID = "_id";
	public static final String CONTENT = "content";
	public static final String TIME = "time";
	public static final String REMINDER = "reminder_time";

	@Override
	public void onCreate(SQLiteDatabase db) {
		/*
		 * Version 1 schema:
		 * create table if not exists notes (
		 *  _id integer primary key,
		 *  content text,
		 *  time integer);
		 */
		final String init_sql = "create table if not exists "+NOTES_TABLE+" ("
				+ ID		+ " integer primary key, "
				+ CONTENT	+ " text, "
				+ TIME		+ " integer, "
				+ REMINDER	+ " integer default null);";
		db.execSQL(init_sql);
		
		Note welcomeNote = new Note();
		String appName = context.getResources().getString(R.string.app_name);
		welcomeNote.setContent("Welcome to "+appName+"!\n\nTo make a new note, go back and touch the + button. When editing a note, your changes are automatically saved.");
		welcomeNote.save(db);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		if(oldVersion >= newVersion) return;
		
		switch(oldVersion){
		case 1:
			// upgrade from 1 to 2
			db.execSQL("alter table "+NOTES_TABLE+" add column "+REMINDER+" integer;");
			if(newVersion == 2) break;
		}
	}

}
