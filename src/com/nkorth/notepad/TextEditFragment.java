package com.nkorth.notepad;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

public class TextEditFragment extends Fragment {

	static final String TAG = "TextEditFragment";
	
	public static final String ARG_ID = "id";
	public static final String ARG_INITIAL_CONTENT = "initial_content";

	public static TextEditFragment newInstance(long id){
		TextEditFragment f = new TextEditFragment();
		Bundle args = new Bundle();
		args.putLong(ARG_ID, id);
		f.setArguments(args);
		return f;
	}
	
	public static TextEditFragment newInstance(String initialContent){
		TextEditFragment f = new TextEditFragment();
		Bundle args = new Bundle();
		args.putString(ARG_INITIAL_CONTENT, initialContent);
		f.setArguments(args);
		return f;
	}
	
	private SQLiteDatabase db;
	private Note note;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		DBStructure dbStructure = new DBStructure(getActivity());
		db = dbStructure.getWritableDatabase();
		if(getNoteId() != null){
			Cursor result = db.query(DBStructure.NOTES_TABLE, null,
					DBStructure.ID+" = ?", new String[]{getNoteId().toString()},
					null, null, null);
			if(result.moveToFirst()){
				note = new Note(result);
			} else{
				Log.e(TAG, "Note id "+getNoteId()+" not found");
				getActivity().finish();
			}
		} else{
			note = new Note();
			if(getArguments() != null && getArguments().containsKey(ARG_INITIAL_CONTENT)){
				note.setContent(getArguments().getString(ARG_INITIAL_CONTENT));
			}
		}
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState){
		super.onActivityCreated(savedInstanceState);
		getActivity().getActionBar().setTitle(getNoteTitle());
	}
	
	private Long getNoteId(){
		if(getArguments() != null && getArguments().containsKey(ARG_ID)){
			return getArguments().getLong(ARG_ID);
		}
		return null;
	}
	
	public String getNoteTitle(){
		String content = note.getContent();
		if(content.length() > 0){
			if(content.indexOf("\n") > 0){
				return content.substring(0, content.indexOf("\n"));
			} else{
				return content;
			}
		} else{
			return getResources().getString(R.string.new_note);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_text_edit, container, false); // don't attach to container yet
		
		EditText textArea = (EditText)rootView.findViewById(R.id.textArea);
		if(note != null){
			textArea.setText(note.getContent());
		}
		
		return rootView;
	}
	
	private void save(){
		EditText textArea = (EditText)getView().findViewById(R.id.textArea);
		String content = textArea.getText().toString();
		note.setContent(content);
		note.save(db);
	}
	
	@Override
	public void onPause(){
		save();
		super.onPause();
	}
	
	public void reminder(){
		save();
		if(note.getContent().equals("")){
			Toast.makeText(getActivity(), R.string.cant_remind_empty, Toast.LENGTH_SHORT).show();
			return;
		}
		if(note.getReminder() == null){
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setTitle(R.string.remind_me);
			builder.setItems(R.array.reminder_times, new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Calendar time = Calendar.getInstance();
					switch(which){
					case 0: // 10min
						time.add(Calendar.MINUTE, 10);
						setReminder(time.getTime());
						break;
					case 1: // 1h
						time.add(Calendar.HOUR_OF_DAY, 1);
						setReminder(time.getTime());
						break;
					case 2: // tonight
						if(time.get(Calendar.HOUR_OF_DAY) > 18){
							// current time is later than 6pm, so go to tomorrow
							time.add(Calendar.DAY_OF_YEAR, 1);
						}
						time.set(Calendar.HOUR_OF_DAY, 18);
						time.set(Calendar.MINUTE, 0);
						time.set(Calendar.SECOND, 0);
						time.set(Calendar.MILLISECOND, 0);
						setReminder(time.getTime());
						break;
					case 3: // custom
						customReminder();
						break;
					}
				}
			});
			builder.create().show();
		} else{
			// reminder already exists
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			String existingTime = DateUtils.formatDateTime(getActivity(), note.getReminder().getTime(),
									DateUtils.FORMAT_SHOW_TIME);
			String existingDate = DateUtils.formatDateTime(getActivity(), note.getReminder().getTime(),
									DateUtils.FORMAT_SHOW_DATE);
			builder.setMessage(getActivity().getString(R.string.reminder_exists, existingDate, existingTime));
			builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.cancel();
				}
			});
			builder.setPositiveButton(R.string.remove_reminder, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					cancelReminder();
				}
			});
			builder.create().show();
		}
	}
	
	private void customReminder(){
		Calendar now = Calendar.getInstance();
		
		TimePickerDialog dialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
			@Override
			public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
				Calendar time = new GregorianCalendar();
				if(hourOfDay == time.get(Calendar.HOUR_OF_DAY) && minute == time.get(Calendar.MINUTE)){
					// time picker was unchanged, so cancel
					return;
				}
				time.set(Calendar.HOUR_OF_DAY, hourOfDay);
				time.set(Calendar.MINUTE, minute);
				time.set(Calendar.SECOND, 0);
				time.set(Calendar.MILLISECOND, 0);
				
				if(time.before(Calendar.getInstance())){
					// time is in the past, so just make it tomorrow
					time.add(Calendar.DAY_OF_YEAR, 1);
				}
				setReminder(time.getTime());
			}
		}, now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), DateFormat.is24HourFormat(getActivity()));
		dialog.show();
	}
	
	private void setReminder(Date time){
		note.setReminder(time);
		save();
		if(note.getReminder() != null){
			ReminderSpawner.setReminder(getActivity(), note);

			CharSequence formattedTime = DateUtils.getRelativeTimeSpanString(time.getTime(), new Date().getTime() - 5000, DateUtils.MINUTE_IN_MILLIS);
			Toast.makeText(getActivity(), getActivity().getString(R.string.reminder_set, formattedTime), Toast.LENGTH_SHORT).show();
		}
	}
	
	private void cancelReminder(){
		note.setReminder(null);
		save();
		AlarmManager alarms = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
		alarms.cancel(note.getReminderPendingIntent(getActivity()));
	}

	@Override
	public void onDestroy(){
		db.close();
		super.onDestroy();
	}
	
}
